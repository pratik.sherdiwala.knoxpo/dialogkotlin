package com.example.dialogdemokotlin

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity :AppCompatActivity(),DFragment.Callback{


    companion object{
        val TAG=MainActivity::class.java.simpleName
        val DIALOG_FRAGMENT="$TAG.DIALOG_FRAGMENT"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        chooseBTN.setOnClickListener(View.OnClickListener {
            val dialog=DFragment()
            dialog.show(supportFragmentManager, DIALOG_FRAGMENT)
        })
    }

    override fun showGender(text: String) {
        answerTV.text=text
    }
}