package com.example.dialogdemokotlin

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import javax.security.auth.callback.Callback

class DFragment : DialogFragment() {

    interface Callback {
        fun showGender(text: String)
    }

    private var mCallback: Callback? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return AlertDialog.Builder(activity)

            .setTitle("Gender Selection")
            .setMessage("What Is Your Gender")
            .setPositiveButton("MALE", DialogInterface.OnClickListener { dialog, which ->
                mCallback?.showGender("MALE")
            })

            .setNegativeButton("FEMALE", DialogInterface.OnClickListener { dialog, which ->
                mCallback?.showGender("FEMALE")
            })

            .create()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mCallback = activity as Callback?
    }

    override fun onDetach() {
        super.onDetach()
        mCallback = null
    }
}